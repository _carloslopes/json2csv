var lineReader = require('line-reader');
var fs = require('fs');

function process(filePath, outputPath) {
  // Le conteudo do arquivo
  //var content = fs.readFileSync(filePath).toString();
  //var content = fs.readFileSync(filePath);

  //console.log("content readed");

  // Processa o json
  var json = jsonFrom(filePath, function(json) {
    console.log("content processed");

    var csv = doCSV(json);
    console.log("content parsed");

    fs.writeFileSync(outputPath, csv);
    console.log("content writted");
  });
}

function jsonFrom(filePath, cb) {
  var result = [];

  var count = 0;

  lineReader.eachLine(filePath, function(line, last) {
    //console.log(count);

    if(last) {
      return;
    }

    if(count != 0) {
      try {
        if(count != 1)
          line = line.toString().slice(1);

        result.push(JSON.parse(line));
      } catch (err) {
        console.log(err);
        console.log(count);
        console.log(line);
      }
    }

    count++;
  }, function() { cb(result) });

  return;

/*
  try {
    result = JSON.parse(string);
  } catch (err) {
    console.log(err);
  }

  if (result == null) {
    console.log("Parse failed, retrying after forcibly quoting keys and removing trailing commas...")
    var relaxed = removeTrailingComma(string);
    try {
      result = JSON.parse(relaxed);
      console.log("Yep: quoting keys and removing trailing commas worked!")
    } catch (err) {
      console.log(err);
    }
  }

  // Try to detect if it's a JSON-lines object - if so, we can parse this.
  //
  // However, this should be TRIED LAST, because this could also modify the
  // CONTENT of the strings (it's not precise enough to only target real
  // line breaks) so if the problem was actually something else, then we want to
  // fix that problem instead. (That said, the string content modification
  // would be minimal -- adding a comma between braces, so that's why I feel
  // okay taking this approach.)
  if ((result == null) && isJSONLines(string)) {
    console.log("Parse failed. Looks like it might be JSON lines, retrying...")
    var lines = linesToJSON(string)
    try {
      result = JSON.parse(lines)
      console.log("Yep: it was JSON lines!")
    } catch (err) {
      console.log(err);
      if (lines.length < 5000) console.log(lines);
    }
  }
  if (result == null)
    console.log("Nope: that didn't work either. No good.")

  return result;
*/
}

function doCSV(json) {
  // 1) find the primary array to iterate over
  // 2) for each item in that array, recursively flatten it into a tabular object
  // 3) turn that tabular object into a CSV row using jquery-csv
  var inArray = arrayFrom(json);

  var outArray = [];
  for (var row in inArray)
      outArray[outArray.length] = parse_object(inArray[row]);


  var csv = fromObjects(outArray);
  return csv;
}

function quoteKeys(input) {
  return input.replace(/(['"])?([a-zA-Z0-9_]+)(['"])?:/g, '"$2": ');
}

function removeTrailingComma(input) {
  if (input.slice(-1).toString() == ",")
    return input.slice(0,-1);
  else
    return input;
}

function isJSONLines(string) {
 return !!(string.match(/\}\s+\{/))
}

function linesToJSON(string) {
  return "[" + string.replace(/\}\s+\{/g, "}, {") + "]";
}

function arrayFrom(json) {
    var queue = [], next = json;
    while (next !== undefined) {
        if (getType(next) == "array") {

            // but don't if it's just empty, or an array of scalars
            if (next.length > 0) {

              var type = getType(next[0]);
              var scalar = (type == "number" || type == "string" || type == "boolean" || type == "null");

              if (!scalar)
                return next;
            }
        } if (getType(next) == "object") {
          for (var key in next)
             queue.push(next[key]);
        }
        next = queue.shift();
    }
    // none found, consider the whole object a row
    return [json];
}

function parse_object(obj, path) {
    if (path == undefined)
        path = "";

    var type = typeof obj;
    var scalar = (type == "number" || type == "string" || type == "boolean" || type == "null");

    if (type == "array" || type == "object") {
        var d = {};
        for (var i in obj) {

            var newD = parse_object(obj[i], path + i + "/");
            Object.assign(d, newD);
        }

        return d;
    }

    else if (scalar) {
        var d = {};
        var endPath = path.substr(0, path.length-1);
        d[endPath] = obj;
        return d;
    }

    // ?
    else return {};
}

function fromObjects(objects, options, callback) {
  var options = (options !== undefined ? options : {});
  var config = {};
  config.callback = ((callback !== undefined && typeof(callback) === 'function') ? callback : false);
  config.separator = 'separator' in options ? options.separator : ',';
  config.delimiter = 'delimiter' in options ? options.delimiter : '"';
  config.headers = 'headers' in options ? options.headers : true;
  config.sortOrder = 'sortOrder' in options ? options.sortOrder : 'declare';
  config.manualOrder = 'manualOrder' in options ? options.manualOrder : [];
  config.transform = options.transform;

  // added by ERIC, return just the arrays
  config.justArrays = 'justArrays' in options ? options.justArrays : false;

  if (typeof config.manualOrder === 'string') {
    config.manualOrder = toArray(config.manualOrder, config);
  }

  if (config.transform !== undefined) {
    var origObjects = objects;
    objects = [];

    var i;
    for (i = 0; i < origObjects.length; i++) {
      objects.push(config.transform.call(undefined, origObjects[i]));
    }
  }

  var props = collectPropertyNames(objects);

  if (config.sortOrder === 'alpha') {
    props.sort();
  } // else {} - nothing to do for 'declare' order

  if (config.manualOrder.length > 0) {

    var propsManual = [].concat(config.manualOrder);
    var p;
    for (p = 0; p < props.length; p++) {
      if (propsManual.indexOf( props[p] ) < 0) {
        propsManual.push( props[p] );
      }
    }
    props = propsManual;
  }

  var o, p, line, output = [], propName;
  if (config.headers) {
    output.push(props);
  }

  for (o = 0; o < objects.length; o++) {
    line = [];
    for (p = 0; p < props.length; p++) {
      propName = props[p];
      if (propName in objects[o] && typeof objects[o][propName] !== 'function') {
        line.push(objects[o][propName]);
      } else {
        line.push('');
      }
    }
    output.push(line);
  }

  // modification by ERIC - just give me the arrays you made out of the object
  if (config.justArrays)
    return output;

  // push the value to a callback if one is defined
  return fromArrays(output, options, config.callback);
}

function toArray(csv, options, callback) {
  var options = (options !== undefined ? options : {});
  var config = {};
  config.callback = ((callback !== undefined && typeof(callback) === 'function') ? callback : false);
  config.separator = 'separator' in options ? options.separator : ',';
  config.delimiter = 'delimiter' in options ? options.delimiter : '"';
  var state = (options.state !== undefined ? options.state : {});

  // setup
  var options = {
    delimiter: config.delimiter,
    separator: config.separator,
    onParseEntry: options.onParseEntry,
    onParseValue: options.onParseValue,
    state: state
  }

  var entry = parseEntry(csv, options);

  // push the value to a callback if one is defined
  if(!config.callback) {
    return entry;
  } else {
    config.callback('', entry);
  }
}

function collectPropertyNames(objects) {
  var o, propName, props = [];
  for (o in objects) {
    for (propName in objects[o]) {
      if ((objects[o].hasOwnProperty(propName))
          && (props.indexOf(propName) < 0)
          && (typeof objects[o][propName] !== 'function')) {

        props.push(propName);
      }
    }
  }
  return props;
}

function fromArrays(arrays, options, callback) {
  var options = (options !== undefined ? options : {});
  var config = {};
  config.callback = ((callback !== undefined && typeof(callback) === 'function') ? callback : false);
  config.separator = 'separator' in options ? options.separator : ',';
  config.delimiter = 'delimiter' in options ? options.delimiter : '"';

  var output = '',
      line,
      lineValues,
      i, j;

  for (i = 0; i < arrays.length; i++) {
    line = arrays[i];
    lineValues = [];
    for (j = 0; j < line.length; j++) {
      var strValue = (line[j] === undefined || line[j] === null)
                   ? ''
                   : line[j].toString();

      // MODIFICATION by Eric:
      // make the delimiter replacement global
      // (workaround until jquery-csv is replaced entirely)
      if (strValue.indexOf(config.delimiter) > -1) {
        var delRegex = new RegExp(config.delimiter, "g");
        strValue = strValue.replace(delRegex, config.delimiter + config.delimiter);
      }

      var escMatcher = '\n|\r|S|D';
      escMatcher = escMatcher.replace('S', config.separator);
      escMatcher = escMatcher.replace('D', config.delimiter);

      if (strValue.search(escMatcher) > -1) {
        strValue = config.delimiter + strValue + config.delimiter;
      }
      lineValues.push(strValue);
    }
    output += lineValues.join(config.separator) + '\r\n';
  }

  // push the value to a callback if one is defined
  if(!config.callback) {
    return output;
  } else {
    config.callback('', output);
  }
}

function parseEntry(csv, options) {
  // cache settings
  var separator = options.separator;
  var delimiter = options.delimiter;

  // set initial state if it's missing
  if(!options.state.rowNum) {
    options.state.rowNum = 1;
  }
  if(!options.state.colNum) {
    options.state.colNum = 1;
  }

  // clear initial state
  var entry = [];
  var state = 0;
  var value = '';

  function endOfValue() {
    if(options.onParseValue === undefined) {
      // onParseValue hook not set
      entry.push(value);
    } else {
      var hook = options.onParseValue(value, options.state); // onParseValue Hook
      // false skips the value, configurable through a hook
      if(hook !== false) {
        entry.push(hook);
      }
    }
    // reset the state
    value = '';
    state = 0;
    // update global state
    options.state.colNum++;
  }

  // checked for a cached regEx first
  if(!options.match) {
    // escape regex-specific control chars
    var escSeparator = RegExp.escape(separator);
    var escDelimiter = RegExp.escape(delimiter);

    // compile the regEx str using the custom delimiter/separator
    var match = /(D|S|\n|\r|[^DS\r\n]+)/;
    var matchSrc = match.source;
    matchSrc = matchSrc.replace(/S/g, escSeparator);
    matchSrc = matchSrc.replace(/D/g, escDelimiter);
    options.match = RegExp(matchSrc, 'gm');
  }

  // put on your fancy pants...
  // process control chars individually, use look-ahead on non-control chars
  csv.replace(options.match, function (m0) {
    switch (state) {
      // the start of a value
      case 0:
        // null last value
        if (m0 === separator) {
          value += '';
          endOfValue();
          break;
        }
        // opening delimiter
        if (m0 === delimiter) {
          state = 1;
          break;
        }
        // skip un-delimited new-lines
        if (m0 === '\n' || m0 === '\r') {
          break;
        }
        // un-delimited value
        value += m0;
        state = 3;
        break;

      // delimited input
      case 1:
        // second delimiter? check further
        if (m0 === delimiter) {
          state = 2;
          break;
        }
        // delimited data
        value += m0;
        state = 1;
        break;

      // delimiter found in delimited input
      case 2:
        // escaped delimiter?
        if (m0 === delimiter) {
          value += m0;
          state = 1;
          break;
        }
        // null value
        if (m0 === separator) {
          endOfValue();
          break;
        }
        // skip un-delimited new-lines
        if (m0 === '\n' || m0 === '\r') {
          break;
        }
        // broken paser?
        throw new Error('CSVDataError: Illegal State [Row:' + options.state.rowNum + '][Col:' + options.state.colNum + ']');

      // un-delimited input
      case 3:
        // null last value
        if (m0 === separator) {
          endOfValue();
          break;
        }
        // skip un-delimited new-lines
        if (m0 === '\n' || m0 === '\r') {
          break;
        }
        // non-compliant data
        if (m0 === delimiter) {
          throw new Error('CSVDataError: Illegal Quote [Row:' + options.state.rowNum + '][Col:' + options.state.colNum + ']');
        }
        // broken parser?
        throw new Error('CSVDataError: Illegal Data [Row:' + options.state.rowNum + '][Col:' + options.state.colNum + ']');
      default:
        // shenanigans
        throw new Error('CSVDataError: Unknown State [Row:' + options.state.rowNum + '][Col:' + options.state.colNum + ']');
    }
    //console.log('val:' + m0 + ' state:' + state);
  });

  // submit the last value
  endOfValue();

  return entry;
}

var class2type = {};
var toString = class2type.toString;

"Boolean Number String Function Array Date RegExp Object Error Symbol".split(" ").forEach(function(name) {
  class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

function getType( obj ) {
  if ( obj == null ) {
    return obj + "";
  }
  return typeof obj === "object" || typeof obj === "function" ?
    class2type[ toString.call( obj ) ] || "object" :
    typeof obj;
}

process('108_orders.json', '108_orders.csv');
